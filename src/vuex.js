import {createStore} from "vuex";

const store = createStore({
    state() {
        return {
            authorized: false,
        }
    },
    mutations: {
        login(authorized) {
            authorized = true
        },
        logout(authorized) {
            console.log("准备执行登出")
            authorized = false
            console.log("登出执行完成")
            console.log(authorized)
        }
    }
})

export default store;