import Mock from "mockjs";
import device from "@/data/deviceInfoData.js"
import blockdata from "@/data/blockInfoData.js"

Mock.mock("/", 'post', () => {
    return {
        status: 200,
        message: 'helloworld'
    }
})

Mock.mock('/api', 'get', () => {
    return {
        status: 404,
        message: 123
    }
})

import deviceExampleData from '@/data/deviceInfoData'
Mock.mock('/deviceinfo', 'get', () => {
    return {
        status: 200,
        data: JSON.stringify(deviceExampleData)
    }
})


import blockExampleData from '@/data/blockInfoData'
Mock.mock('/blockinfo','get',()=>{
    return {
        status: 200,
        data: JSON.stringify(blockExampleData)
    }
})

export default Mock