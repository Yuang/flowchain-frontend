import {createApp} from 'vue'
import './tailwind.css'
import App from './App.vue'
import {routes} from './routes.js'
import {createRouter, createWebHistory} from 'vue-router'
import ElementPlus from 'element-plus'
import 'element-plus/lib/theme-chalk/index.css'
import axios from 'axios'
import Mock from '@/mock.js'
import store from '@/vuex.js'
// import cookie from 'vue-cookie'


let app = createApp(App)
app.config.globalProperties.$axios = axios
let router = createRouter({
    history: createWebHistory(),
    routes: routes
})


app.use(router)
    .use(store)
    .use(ElementPlus)
    .use(Mock)
    // .use(cookie)
app.mount('#app')