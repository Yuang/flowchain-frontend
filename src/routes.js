import Home from './views/Home.vue'
// import About from './views/About.vue'
import NotFound from './views/NotFound.vue'

/** @type {import('vue-router').RouterOptions['routes']} */
export let routes = [
  { path: '/', component: Home, meta: { title: 'Home' } },
  {
    path: '/about',
    meta: { title: 'About' },
    // example of route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('./views/About.vue'),
    // component: About
  },{
    path: '/test',
    meta: {title: 'Test'},
    component: () => import('./views/Test.vue'),
  }, {
    path: '/hall',
    meta: {title: 'Hall'},
    component: () => import('./views/Hall.vue'),
  },{
    path: '/example',
    meta: {title: 'Example'},
    component: () => import('./views/example.vue'),
  },{
    path: '/overview',
    meta: {title: 'overview'},
    component: ()=> import('./views/overview.vue'),
  },{
    path: '/chaininfo',
    meta: {title: 'chaininfo'},
    component: () => import('./views/chaininfo.vue'),
  },{
    path: '/deviceinfo',
    meta: {title: 'deviceinfo'},
    component: () => import('./views/deviceinfo.vue')
  },{
    path: '/config',
    meta: {title: 'config'},
    component: () => import('./views/config.vue'),
  }, { path: '/:path(.*)', component: NotFound },
]
